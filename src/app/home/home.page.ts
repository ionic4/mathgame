import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  num1: number;
  num2: number;
  answer: any;

  constructor(public alertController: AlertController) {}

  ngOnInit() {
    this.num1 = Math.floor((Math.random() * 10) + 1);
    this.num2 = Math.floor((Math.random() * 10) + 1);
  }

  check() {
    if (this.answer === (this.num1 + this.num2)) {
      this.alert('Correct', 'You answer is correct!');
    } else {
      this.alert('Wrong', 'Your answer is wrong!');
    }
    this.num1 = Math.floor((Math.random() * 10) + 1);
    this.num2 = Math.floor((Math.random() * 10) + 1);
    this.answer = '';
  }

  private async alert(title: string, text: string) {
    const alert = await this.alertController.create({
      header: 'Math game',
      subHeader: title,
      message: text,
      buttons: ['OK']
    });

    alert.present();
  }
}
